package com.gmail.frankkusmiruk.settei;

import ch.njol.skript.Skript;
import ch.njol.skript.SkriptAddon;
import org.bukkit.plugin.java.JavaPlugin;

public class Settei extends JavaPlugin {


    private static Settei instance;
    private static SkriptAddon addonInstance;

    private Settei() {
    }


    @Override
    public void onEnable() {

    }

    public static SkriptAddon getAddonInstance() {
        if (addonInstance == null) {
            addonInstance = Skript.registerAddon(getInstance());
        }
        return addonInstance;
    }

    public static Settei getInstance() {
        if (instance == null) {
            instance = new Settei();
        }
        return instance;
    }

}
