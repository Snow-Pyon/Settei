package com.gmail.frankkusmiruk.settei.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import org.bukkit.event.Event;

public class EffMapToml extends Effect {

    static {
        Skript.registerEffect(EffMapToml.class,
                "(map|copy) [the] toml [(of|from)] %string% to [the] [var[iable]] %objects%");
    }

    @Override
    protected void execute(Event e) {

    }

    @Override
    public String toString(Event e, boolean debug) {
        return null;
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        return false;
    }
}
